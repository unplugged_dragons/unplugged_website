from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.edit import FormView
from django.contrib.auth import views as auth_views

from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from .forms import RegisterForm, LoginForm, PasswordChangeForm
from .models import User

# Create your views here.

class PasswordChangeView(auth_views.PasswordChangeView):
    form_class = PasswordChangeForm
    template_name='users/password_change.html'
    success_url = reverse_lazy("home")

class LoginView(auth_views.LoginView):

    form_class = LoginForm
    success_url = reverse_lazy("home")
    template_name = "users/login.html"


class RegisterView(FormView):

    template_name = "users/registration.html"
    form_class = RegisterForm

    success_url = reverse_lazy("users:login")

    def form_valid(self, form):
        form.save(self.request)

        return super().form_valid(form)


def activate_account(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        # messages.add_message(request, messages.INFO, 'Account activated. Please login.')
    else:
        # messages.add_message(request, messages.INFO, 'Link Expired. Contact admin to activate your account.')
        pass

    return redirect("users:login")
