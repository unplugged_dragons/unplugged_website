# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import math

from django.contrib.auth.models import AbstractUser
from django.db import models

from pinax.points.models import points_awarded
from pinax.points.signals import points_awarded as points_awarded_signal

class User(AbstractUser):
    display_name = models.CharField(max_length=64)
    level = models.IntegerField(default = 1)
    public_profile = models.BooleanField(default=True)

    def __str__(self):
        return self.display_name or self.username


    @property
    def experience(self):
        return points_awarded(self)

    @property
    def next_level_threshold(self):
        return User.compute_level_treshold(self.level+1)

    @property
    def next_level_progress(self):
        base_treshold = User.compute_level_treshold(self.level)
        exp = self.experience - base_treshold
        required = self.next_level_threshold - base_treshold
        return int(exp / required * 100)

    @property
    def latest_awards(self):
        return self.awardedpointvalue_targets.all()[:5]


    def update_level(self):
        expected_level = int((1 + math.sqrt(1 + 8 * self.experience / 50)) / 2)

        while self.level < expected_level:
            self.level += 1
            # send notification

        self.save()

    @staticmethod
    def compute_level_treshold(level):
        return ((level ** 2 - level) * 50 ) // 2


def user_level_up(target, key, points, source, **kwargs):

    print(target, key, points, source)
    if isinstance(target, User):
        target.update_level()

points_awarded_signal.connect(user_level_up)

