from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

class CalendarView(LoginRequiredMixin, TemplateView):
    template_name = 'events/calendar.html'

