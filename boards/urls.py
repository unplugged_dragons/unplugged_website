"""boards URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from .views import HomeView
from shops.views import OffersList

from modernrpc.views import RPCEntryPoint
from modernrpc.handlers import jsonhandler

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('games/', include('games.urls', namespace='games')),
    path('shops/', include('shops.urls', namespace='shops')),
    path('deals/', OffersList.as_view(), name='deals'),
    path('users/', include('users.urls', namespace='users')),
    path('events/', include('events.urls', namespace='events')),
    path('groups/', include('groups.urls', namespace='groups')),
    path('admin/', admin.site.urls),
    # Pinax
    path("ratings/", include("ratings.urls", namespace="ratings")),
    path("api/v1/", include("games.rpc_urls", namespace="ratings"))
]



