from django import forms

from .models import ShopModel

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Field, Submit, Button, Div
from crispy_forms.bootstrap import FormActions

class ShopForm(forms.ModelForm):

    class Meta:
        model = ShopModel
        fields = ['name', 'short_description', 'website', 'facebook', 'elastic_name']

    def __init__(self, *args, **kwargs):
        super(ShopForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Field('name', placeholder='Enter shop name'),
            Field('short_description', placeholder='Enter short description (optional)'),
            Field('website', placeholder='Enter site url'),
            Field('facebook', placeholder='Enter facebook fanpage url (optional)'),
            Field('elastic_name', placeholder='Enter elasticsearch name'),
            FormActions(
                Submit('submit', 'Save'),
                Submit('verify', 'Save &amp; Verify'),
                #Submit('cancel', 'Cancel', css_class='btn btn-danger')
            )
        )

class SuggestShopForm(forms.ModelForm):

    class Meta:
        model = ShopModel
        fields = ['name', 'website']

    def __init__(self, *args, **kwargs):
        super(SuggestShopForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Div(
                Div(
                    Field('name', placeholder='Enter shop name', css_class='form-control', template='layouts/baseinput.html'),
                    Field('website', placeholder='Enter site url', css_class='form-control mg-md-l-10 mg-t-10 mg-md-t-0', template='layouts/baseinput.html'),
                    Submit('submit', 'Recommend', css_class='btn btn-info pd-y-13 pd-x-20 bd-0 mg-md-l-10 mg-t-10 mg-md-t-0 tx-uppercase tx-11 tx-spacing-2'),
                    css_class='d-md-flex pd-y-20 pd-md-y-0'
                ),
                css_class='d-flex align-items-center justify-content-center ht-md-80 pd-x-20'
            )
        )
