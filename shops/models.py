from datetime import datetime
import itertools

from django.db import models
from django.conf import settings
from django.utils.text import slugify
# Create your models here.


class ShopModel(models.Model):

    name = models.CharField(max_length=64)
    slug = models.SlugField(max_length=64, unique=True)
    elastic_name = models.SlugField(max_length=64, unique=True, blank=True, null=True)
    #description = models.TextField(blank=True, default='') # tTODO: remove this field
    short_description = models.CharField(max_length=256, blank=True, default='')
    website = models.URLField(max_length=64)
    facebook = models.URLField(max_length=64, blank=True, null=True)
    verified_at = models.DateTimeField(null=True)
    verified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, related_name='verified_shops')

    deleted_at = models.DateTimeField(null=True)
    deleted_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, related_name='deleted_shops')

    def save(self, *args, **kwargs):

        if not self.slug:
            max_length = self._meta.get_field('slug').max_length
            slug_candidate = slug_original = slugify(self.name, allow_unicode=False)
            Klass = self.__class__
            for i in itertools.count(1):
                if not Klass.objects.filter(slug=slug_candidate).exists():
                    break
                suffix = f"-{i}"
                suffix_length = len(suffix)
                slug_base = slug_original[:max_length - suffix_length]
                slug_candidate = ''.join([slug_base, suffix])
            self.slug = slug_candidate
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):

        self.deleted_at = datetime.now()
        self.save()

    def __str__(self):
        return self.name

class UserPreferedShops(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='preferd_shops'
    )
    shop = models.ForeignKey(
        ShopModel,
        on_delete=models.CASCADE,
        related_name='dedicated_customers',
    )
    joined_at = models.DateTimeField(auto_now=True, null=False)
    unsubscribed_at = models.DateTimeField(null=True)
