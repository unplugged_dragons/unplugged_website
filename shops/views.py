from datetime import datetime, timedelta

from django.shortcuts import render, redirect
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from django.db.models import Count, Q
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect

# Create your views here.

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

from .forms import ShopForm, SuggestShopForm
from .models import ShopModel, UserPreferedShops


class ListShopsView(LoginRequiredMixin, ListView):

    model = ShopModel
    template_name = "shops/verified_list.html"
    context_object_name = "shops"

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .exclude(verified_by=None)
            .annotate(likes_count=Count("dedicated_customers"))
            .annotate(
                user_choice=Count(
                    "dedicated_customers",
                    filter=Q(dedicated_customers__user=self.request.user),
                )
            )
            .order_by("-likes_count", "name")
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["suggestion_form"] = SuggestShopForm()

        # if self.request.user.has_perm('shops.can_verify'):
        if True:
            context["awaiting_confirmation"] = (
                super().get_queryset().filter(verified_by=None, deleted_at=None)
            )
        return context

    def post(self, request, *args, **kwargs):
        self.suggestion_form = SuggestShopForm(self.request.POST or None)
        if self.suggestion_form.is_valid():
            self.suggestion_form.save()

        return redirect(reverse("shops:list"))


class CreateShop(LoginRequiredMixin, CreateView):
    model = ShopModel
    template_name = "shops/form.html"
    form_class = ShopForm
    success_url = "/"


class UpdateShop(LoginRequiredMixin, UpdateView):
    model = ShopModel
    template_name = "shops/form.html"
    form_class = ShopForm
    success_url = "/"

    def get_form(self, form_class=None):
        form = super().get_form(form_class)

        if "verify" in self.request.POST:
            form.instance.verified_at = datetime.now()
            form.instance.verified_by = self.request.user

        return form


class SwitchFavouriteShop(LoginRequiredMixin, View):
    def get(self, request, slug):

        shop = ShopModel.objects.get(slug=slug)
        try:
            preference = UserPreferedShops.objects.get(user=request.user, shop=shop)
            preference.delete()
        except UserPreferedShops.DoesNotExist:
            UserPreferedShops(user=request.user, shop=shop).save()

        return redirect(reverse_lazy("shops:list"))


class ShopDelete(LoginRequiredMixin, DeleteView):
    model = ShopModel
    template_name = "shops/shop_confirm_delete.html"
    success_url = reverse_lazy("shops:list")

    def delete(self, request, *args, **kwargs):

        """
        Call the delete() method on the fetched object and then redirect to the
        success URL.
        """
        self.object = self.get_object()
        self.object.deleted_by = self.request.user
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)


class OffersList(LoginRequiredMixin, TemplateView):

    template_name = "shops/offers/list.html"

    def get_context_data(self, **kwargs):
        params = self.request.GET

        sort_orders = {
            "default": ("-discount_percentage",),
            "deal": ("-discount_percentage",),
            "discount": ("-discount_value",),
            "price": ("price", "-discount_percentage"),
        }

        context = super().get_context_data(**kwargs)

        client = Elasticsearch()

        search = (
            Search(using=client, index=f"test-products-{params.get('shop', '*')}")
            .filter("range", discount_percentage={"gt": "0.0", "lte": 100.0})
            .filter(
                "range", _meta__last_update={"gt": datetime.now() - timedelta(days=1)}
            )  # Filter only currently updated offers
            .sort(*sort_orders.get(params.get("sort", "default")))
            .exclude("match", out_of_stock=True)
        )

        q = params.get("q")
        if q:
            search = search.filter("match", name=q)

        # pagination
        pagesize = int(params.get("pagesize", 100))
        page = int(params.get("page", 1))
        search = search[(page - 1) * pagesize : page * pagesize]

        context["offers"] = search.execute()

        print(context["offers"])

        return context
