# Generated by Django 2.2 on 2019-05-14 20:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shops', '0008_shopmodel_short_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shopmodel',
            name='description',
        ),
    ]
