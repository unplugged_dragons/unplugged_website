$(function () {
    // Assumptions:
    // 1. you have a div with the id of "user_rating" where you want the stars to go
    // 2. you have a container with the class .overall_rating where the new average rating will go
    $("#user_rating").raty({
        score: function() {
            return $(this).attr('data-score');
        },
        readOnly: function() {
            return parseInt($(this).attr('data-score')) != 0;
        },
        click: function(score, evt) {
            var current_rating = 0;
            if (score != null) {
                current_rating = score;
            }
            console.log("Score", score, "{{ post_url }}");
            $.ajax({
                url: "{{ post_url }}",
                type: "POST",
                data: {
                    "rating": current_rating
                    {% if category %}
                    ,"category": "{{ category }}"
                    {% endif %}
                },
                statusCode: {
                    403: function(jqXHR, textStatus, errorThrown) {
                        // Invalid rating was posted or invalid category was sent
                        console.log(errorThrown);
                    },
                    200: function(data, textStatus, jqXHR) {
                        {% if category %}
                            $(".overall_rating.category-{{ category }}").text(data["overall_rating"]);
                        {% else %}
                            $(".overall_rating").text(data["overall_rating"]);
                        {% endif %}
                    }
                }
            });
        },
        cancel: true,
        path: "{{ STATIC_URL }}pinax/ratings/img/"
    });
});
