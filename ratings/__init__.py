import pkg_resources


__version__ = '1.0.0'


default_app_config = "ratings.apps.AppConfig"
