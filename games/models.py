from django.db import models
from core.models import SlugifiedModelMixin

# Create your models here.


class Mechanic(SlugifiedModelMixin, models.Model):
    name = models.CharField(max_length=64)
    bgg_id = models.IntegerField()
    description = models.TextField()


class Genre(SlugifiedModelMixin, models.Model):
    name = models.CharField(max_length=64)
    bgg_id = models.IntegerField()
    description = models.TextField()


class Publisher(SlugifiedModelMixin, models.Model):
    name = models.CharField(max_length=64)
    bgg_id = models.IntegerField()
    description = models.TextField()


class Category(SlugifiedModelMixin, models.Model):
    name = models.CharField(max_length=64)
    bgg_id = models.IntegerField()
    description = models.TextField()


class Game(SlugifiedModelMixin, models.Model):
    name = models.CharField(max_length=128)
    thumbnail = models.ImageField()
    bgg_id = models.IntegerField(blank=True, null=True)
    year_published = models.IntegerField()
    min_players = models.IntegerField()
    max_players = models.IntegerField()
    playing_time = models.IntegerField()
    age = models.IntegerField()
    mechanics = models.ManyToManyField(Mechanic)  # , on_delete=models.CASCADE)
    publishers = models.ManyToManyField(Publisher)  # , on_delete=models.CASCADE)
    genres = models.ManyToManyField(Genre)  # , on_delete=models.CASCADE)
