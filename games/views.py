from django.shortcuts import render

from django.views.generic import TemplateView, ListView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from games.models import Game
from ratings import views as ratings_views

class ListGamesView(LoginRequiredMixin, ListView):

    model = Game
    context_object_name = "games"

    def get_queryset(self):
        return super().get_queryset()

class RateView(ratings_views.RateView):
    model = Game

