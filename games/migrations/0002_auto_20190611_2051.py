# Generated by Django 2.2 on 2019-06-11 20:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='bgg_id',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
