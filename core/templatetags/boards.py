from datetime import datetime, timedelta
import re

from django import template
from django.urls import reverse, NoReverseMatch

register = template.Library()

@register.filter(name='short_datetime')
def short_datetime(value):
    if value.date() == datetime.today().date():
        return value.strftime("%H:%M:%S")
    elif value.date() == (datetime.today() - timedelta(days=1)).date():
        return 'yesterday'

    return value.strftime("%Y-%m-%d")



@register.simple_tag(takes_context=True)
def active(context, pattern_or_urlname):
    try:
        pattern = '^' + reverse(pattern_or_urlname)
    except NoReverseMatch:
        pattern = pattern_or_urlname
    path = context['request'].path
    if re.search(pattern, path):
        return 'active'
    return ''
