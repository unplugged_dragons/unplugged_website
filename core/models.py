from django.db import models
from django.utils import timezone
from django.utils.text import slugify
import itertools

# Create your models here.


class TimedModelMixin(models.Model):
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()

    def save(self, *args, **kwargs):
        """ Initialize `created` model field while models object is created"""

        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(TimedModelMixin, self).save(*args, **kwargs)

    class Meta:
        abstract = True

class SlugifiedModelMixin(models.Model):
    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        """ Initialize `created` model field while models object is created"""

        if not self.slug:
            max_length = self._meta.get_field('slug').max_length
            slug_candidate = slug_original = slugify(self.name, allow_unicode=False)
            Klass = self.__class__
            for i in itertools.count(1):
                if not Klass.objects.filter(slug=slug_candidate).exists():
                    break
                suffix = f"-{i}"
                suffix_length = len(suffix)
                slug_base = slug_original[:max_length - suffix_length]
                slug_candidate = ''.join([slug_base, suffix])
            self.slug = slug_candidate

        if not self.id:
            self.created = timezone.now()

        return super(SlugifiedModelMixin, self).save(*args, **kwargs)

    class Meta:
        abstract = True

